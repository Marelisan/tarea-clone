﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo01_Encapsulacion
{
    public interface ITratamientoDeDatos
    {
        void MostarDatos();
        void PedirDatos();
    }
}
