﻿using Ejemplo01_Encapsulacion;
using Ejemplo02_Herencia;
using Ejercicio01_Encapsulacion;
using System;


namespace Ejemplo03_Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            // Por lo general no es aconsejable crear un array de objetos
            // OJO: Esto es sólo a nivel educativo
            // Lo normal es declarar un array de clases, interfaces ó  clases abstractas
            object[] popurri = new object[3];
            popurri[0] = new Coche("Fiat", "Punto", 9000);
            popurri[1] = new CocheElectrico("Fiat", "Punto", 9000);
            popurri[2] = new Usuario("Fulanito", 50, 2);

            foreach (object objQueSea in popurri)
            {
                Console.WriteLine(objQueSea.ToString());
            }
            ((Coche)popurri[0]).SetNombre("FIAT - PUNTO 4.5");
            Coche fiatPunto = (Coche) popurri[0];
            Console.WriteLine(fiatPunto.GetNombre());
            // El polimorfismo se puede usar con interfaces
            INombrable fiatNombrable = /*(INombrable)*/ fiatPunto;
            fiatNombrable.Nombre = "Fiat - Punto Version 1034";
            Console.WriteLine(fiatNombrable.Nombre);

            INombrable cen = (CocheElectrico)popurri[1];
            Console.WriteLine(cen.Nombre);
        }
    }
}
