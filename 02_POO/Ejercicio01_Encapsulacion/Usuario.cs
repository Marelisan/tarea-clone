﻿using Ejemplo03_Interfaces;
using Ejemplo01_Encapsulacion;
using System;



namespace Ejercicio01_Encapsulacion
{

    /* Nuevo Proyecto Ejercicio01_Encapsulacion
     *  Crear una clase Usuario en otro fichero, con nombre, edad, y altura, 
     *  pero con  las variables miembro encapsuladas como propiedades
     *  Nombre no puede ser ni null ni "". En su lugar "SIN NOMBRE"
     *  edad debe ser mayor que 0
     *  Altura mínima de 0.1F metros
     *  
     *  Crear un pequeño código para comprobar que funciona:
     *      Con casos que funcione bien y casos con valores prohibidos
     */
    public class Usuario : Object, INombrable, ITratamientoDeDatos
    {
        private string nombre;
        private int edad;
        private float altura;

        public string Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                    nombre = "SIN NOMBRE";
                else
                    nombre = value;
            }
        }
        public int Edad
        {
            get
            {
                return edad;
            }
            set
            {
                edad = value <= 0 ? 1 : value;
            }
        }
        public float Altura
        {
            get
            {
                return altura;
            }
            set
            {
                altura = value <= 0.1f ? 0.1f : value;
            }
        }

        public Usuario(string nombre, int edad, float altura)
        {
            this.Nombre = nombre;
            this.Edad = edad;
            this.Altura = altura;
        }
        public override string ToString()
        {
            return "Usuario " + this.Nombre
                + ", " + this.Edad + " años, " + Altura + " m";
        }

        public string GetNombre()
        {
            return "Nombre: " + Nombre;
        }

        public void SetNombre(string unNombre)
        {
            if (string.IsNullOrEmpty(unNombre))
                this.nombre = "SIN NOMBRE";
            else
                this.nombre = unNombre;
        }

        public virtual void MostarDatos()
        {
            Console.WriteLine("Nombre: " + Nombre);
            Console.WriteLine("Edad: " + Edad);
            Console.WriteLine("Altura: " + Altura);

        }

        public virtual void PedirDatos()
        {
            Console.WriteLine("Ingrese el nombre: ");
            Nombre = Console.ReadLine();
            Console.WriteLine("Ingrese la edad: ");
            Edad = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese la atura: ");
            Altura = float.Parse(Console.ReadLine());
        }
    }
}
