﻿using System;
using Ejemplo03_Interfaces;
using Ejercicio01_Encapsulacion;
using Ejercicio02_Herencia;
using Ejemplo02_Herencia;

namespace Ejercicio03_Interfaces
{
    /* 1 - Comprobar si Coche Electrico puede hacerse polimorfismo con INom...
 * 2 - Podeis un nuevo proyecto Ejercicio03_interfaces
 *      Hacer que el usuario que ya tenemos implemente la interfaz 
 *      INombrable y usar los 2 métodos y la propiedad
 * 3 -  Crear un array de INombrable con un empleado y su coche electrico
 * 4 -  Implementar la interfaz IClonable en CocheElectrico:
 *      El método tiene que instanciar un nuevo obj y asignar 
 *      las propiedades del nuevo coche con sus propias propiedades
 * 5 - Crear una nueva interfaz que obligue a implementar 2 métodos, uno para
 *      mostrar directamente los datos por consola y otro para pedir sus datos
 *      por consola
 * 6.- Implementar dicha interfaz en Usuario y en Coche, sobreescribir los 
 *     métodos en empleado (si queréis en CElectrico)
 * 7.- Usar los métodos en un nuevo usuario y en el empleado del ejercicio 3, y en un coche (y si queréis 
 *  CocheElectrico).
 */
    class Program
    {
        static void Main(string[] args)
        {
            INombrable[] empleado = new INombrable[2];
            Empleado empleado1 = new Empleado("Tere",48,1.60F,1500);
            empleado[0] = empleado1;
            CocheElectrico cocheElec = new CocheElectrico("Mercedes","Uno", 30000F, 100);
            empleado[1] = cocheElec;
            Console.WriteLine("Coche inicial: " + cocheElec.ToString());
            CocheElectrico cocheElec2 = new CocheElectrico();
            Console.WriteLine("Coche creado con constructor por defcto: " + cocheElec2.ToString());
            cocheElec = (CocheElectrico)cocheElec2.Clone();
            Console.WriteLine("Coche inicial clonado: " + cocheElec.ToString());
            empleado1.PedirDatos();
            empleado1.MostarDatos();




        }
    }
}
