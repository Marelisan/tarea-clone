﻿using System;
//Crear una estructura ProductoE independiente (NO anidada) con nombre y precio y su constructor 
//con una función para mostrar sus datos

// Crear una clase ProductoC independiente (NO anidada) con nombre y precio y su constructor 
//con una función para mostrar sus datos

//Crear 4 funciones staticas en Programa:
//  -Una que reciba ProductoE y modifique su nombre y su precio
//  -Otra que reciba una ref ProductoE y modifique su nombre y su precio
//  -Una que reciba ProductoC y modifique su nombre y su precio
//  -Otra que reciba una ref ProductoC y modifique su nombre y su precio

//Por último, en Main(), comprobar el comportamiento para saber cual modificar realmente la variable original.


/*
 * 1.- Las estructuras siempre se pasan por valor a menos que se indique a posta con ref que es por referencia
 * 2.- No pueden heredar una de otras
 * 3.- No pueden haber estructuras estáticas
 * 4.- Siempre tienen un construstor por defecto
 * 
 * 1.- Las clases siempre se pasan por referencia. Es redundante usar ref
 * 2.- Si pueden heredar una de otras
 * 3.- Si pueden haber estructuras estáticas
 * 4.- Solo tienen un construstor por defecto cuando no hemos creado un constructor explícitamente
 * */

namespace EjemploEstruc_Class
{
    class Program
    {
        static void Main(string[] args)
        {
            // Para productos trabajados con la estructura sin ref
            ProductoE prodE = new ProductoE("gorro", 1.5f);
            prodE.MostrarDatoEs();
            prodE= ModificaNomPrecioE(prodE);
            prodE.MostrarDatoEs();

            // Para productos trabajados con la estructura con ref
            ProductoE prodEr = new ProductoE("vestido", 4.5f);
            prodEr.MostrarDatoEs();
            ModificaNomPrecioER(ref prodEr);
            prodEr.MostrarDatoEs();


            // Para productos trabajados con la clase sin ref
            ProductoC prodC = new ProductoC("detergente", 8.5f);
            prodC.MostrarDatosC();
            prodC = ModificaNomPrecioC(prodC);
            prodC.MostrarDatosC();

            // Para productos trabajados con la clase con ref
            ProductoC prodCr = new ProductoC("limpiador", 4.5f);
            prodCr.MostrarDatosC();
            ModificaNomPrecioCR(ref prodCr);
            prodCr.MostrarDatosC();

            
        }

        //Métodos para la estructura
        public static ProductoE ModificaNomPrecioE(ProductoE producto)
        {
            string nombre;
            float precio;
            Console.WriteLine("Escriba el nuevo nombre: ");
            nombre = Console.ReadLine();
            Console.WriteLine("Escriba el nuevo precio: ");
            precio = float.Parse(Console.ReadLine());
            producto = new ProductoE(nombre, precio);
            return producto;
        }

        public static void  ModificaNomPrecioER(ref ProductoE producto)
        {
            string nombre;
            float precio;
            Console.WriteLine("Escriba el nuevo nombre: ");
            nombre = Console.ReadLine();
            Console.WriteLine("Escriba el nuevo precio: ");
            precio = float.Parse(Console.ReadLine());
            producto = new ProductoE(nombre, precio);
        }

        //Métodos para la Clase

        public static ProductoC ModificaNomPrecioC(ProductoC producto)
        {
            string nombre;
            float precio;
            Console.WriteLine("Escriba el nuevo nombre: ");
            nombre = Console.ReadLine();
            Console.WriteLine("Escriba el nuevo precio: ");
            precio = float.Parse(Console.ReadLine());
            producto = new ProductoC(nombre, precio);
            return producto;
        }

        public static void ModificaNomPrecioCR(ref ProductoC producto)
        {
            string nombre;
            float precio;
            Console.WriteLine("Escriba el nuevo nombre: ");
            nombre = Console.ReadLine();
            Console.WriteLine("Escriba el nuevo precio: ");
            precio = float.Parse(Console.ReadLine());
            producto = new ProductoC(nombre, precio);
        }


        //Estructura
        public struct ProductoE
        {
            string nombre;
            float precio;
            public ProductoE(string nombre, float precio)
            {
                this.nombre = nombre;
                this.precio = precio;
            }
            public void MostrarDatoEs()
            {
                Console.WriteLine("Nom: " + nombre + ", Precio: " + precio);
            }
        }

        //Clase
        public class ProductoC
        {
            string nombre;
            float precio;
            public ProductoC(string nombre, float precio)
            {
                this.nombre = nombre;
                this.precio = precio;
            }
            public void MostrarDatosC()
            {
                Console.WriteLine("Nom: " + nombre + ", Precio: " + precio);
            }
        }
    }
}
