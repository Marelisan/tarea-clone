﻿using System;

namespace Ejemplo12
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
            string texto = "    En algún lugar de la mancha de cuyo nombre nombre me quiero acordar";
            Console.WriteLine("Original "+ texto);
            Console.WriteLine("Sin espacios: " + texto.Trim());
            Console.WriteLine("Mayus: " + texto.ToUpper());
            Console.WriteLine("Minus: " + texto.ToLower());
            Console.WriteLine("Un cacho: " + texto.Substring(20,20));
            Console.WriteLine("Un cacho hasta el final: " + texto.Substring(20, 20));
            Console.WriteLine("La mancha? " + texto.IndexOf("la mancha")); // IndexOf distingue entre mayúsculas y minúsculas

            string[] palabras = texto.Trim().Split(" ");
            Console.WriteLine("Palabra por palabra: ");
            for (int p = 0; p < palabras.Length;p++)
            {
                Console.WriteLine("Palabra " + p + ": " + palabras[p]);
            }
            Console.WriteLine("Por Pamplona:"+texto.Replace("La mancha","Pamplona").Replace("no","Sarriguren"));
            */

            //Ejercicio: pedir al usuario 3 palabras y juntarlas en una única variable de tipo texto, separandolas por comas y sin espacios
            /*Mi solución
            int contador = 0;
            string palabra1, palabra2, palabra3, frase;
            Console.WriteLine("Ingrese palabra 1: ");
            palabra1 = Console.ReadLine();
            Console.WriteLine("Ingrese palabra 2: ");
            palabra2 = Console.ReadLine();
            Console.WriteLine("Ingrese palabra 3: ");
            palabra3 = Console.ReadLine();
            frase = palabra1.Trim()+ palabra2.Trim()+ "," + palabra3.Trim();
            Console.WriteLine(frase);
            */

            //solución del profesor
            palabras = new string[3];
            for (int p = 0; p < palabras.Length; p++)
            {
                Console.WriteLine("Palabra " + p + ": " + palabras[p]);
            }
            



        }
    }
}
